package me.JoshuaDavis.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.JoshuaDavis.data.Component;
import me.JoshuaDavis.data.InputContainer;
import me.JoshuaDavis.data.ViolationContainer;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.*;

public class SatelliteManager {

    private final Map<Integer, Map<Component, ViolationContainer>> satelliteViolationsMap = new HashMap<>();
    private final List<ViolationContainer> alerts = new ArrayList<>();
    private final int VIOLATION_INTERVAL = 5; //in minutes

    public void ingest(File file) {
        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                InputContainer inputData = new InputContainer(scan.nextLine()); //POJO with data
                if (isViolating(inputData)) handleViolation(inputData);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error reading data: File is invalid.");
        }
    }

    public void pushAlerts() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(alerts));
        alerts.clear();
    }

    private boolean isViolating(InputContainer container) {
        switch (container.getComponent()) {
            case BATT:
                return container.getValue() < container.getRedLow();
            case TSTAT:
                return container.getValue() > container.getRedHigh();
            default:
                return false;
        }
    }

    private void handleViolation(InputContainer inputData) {
        int ID = inputData.getID();
        Component component = inputData.getComponent();

        Map<Component, ViolationContainer> violations = satelliteViolationsMap.getOrDefault(ID, new HashMap<>());
        ViolationContainer violation = violations.get(inputData.getComponent());

        if (violation == null) violations.put(component, new ViolationContainer(inputData));
        else {
            long mins = Duration.between(violation.getStamp(), inputData.getStamp()).toMinutes();
            if (mins < VIOLATION_INTERVAL) violation.incrementViolations();
            else violations.put(component, new ViolationContainer(inputData));
        }

        satelliteViolationsMap.put(ID, violations);
    }

    public void alertViolation(ViolationContainer container) {
        alerts.add(container);
    }
}