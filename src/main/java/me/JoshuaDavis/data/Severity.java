package me.JoshuaDavis.data;

public enum Severity {
    RED_LOW,
    RED_HIGH;

    public static Severity fromComponent(Component component) {
        switch (component) {
            case BATT:
                return RED_LOW;
            case TSTAT:
                return RED_HIGH;
            default:
                return null;
        }
    }
}
