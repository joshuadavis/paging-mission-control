package me.JoshuaDavis.data;

import me.JoshuaDavis.Main;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ViolationContainer {

    /*
     * Fields that will be formatted by gson
     */
    private final int satID;
    private final Severity severity;
    private final Component component;
    private final String timestamp;

    /*
     * Transient fields to be ignored by gson formatting
     */
    private transient final ZonedDateTime stamp;
    private transient int violations = 1;

    public ViolationContainer(int ID, Component component, ZonedDateTime stamp) {
        this.satID = ID;
        this.severity = Severity.fromComponent(component);
        this.component = component;
        this.timestamp = stamp.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        this.stamp = stamp;
    }
    public ViolationContainer(InputContainer container) {
        this(container.getID(), container.getComponent(), container.getStamp());
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public void incrementViolations() {
        violations++;
        if (violations == 3) Main.getSatelliteManager().alertViolation(this);
    }
}
