package me.JoshuaDavis.data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class InputContainer {

    private final ZonedDateTime stamp;
    private final int ID;
    private final double redHigh;
    private final double yellowHigh;
    private final double yellowLow;
    private final double redLow;
    private final double value;
    private final Component component;

    public InputContainer(String string) {
        String[] rawData = string.split("\\|");

        stamp = LocalDateTime.parse(rawData[0], DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS")).atZone(ZoneId.of("Zulu"));
        ID = Integer.parseInt(rawData[1]);
        redHigh = Double.parseDouble(rawData[2]);
        yellowHigh = Double.parseDouble(rawData[3]);
        yellowLow = Double.parseDouble(rawData[4]);
        redLow = Double.parseDouble(rawData[5]);
        value = Double.parseDouble(rawData[6]);
        component = Component.valueOf(rawData[7].toUpperCase());
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public int getID() {
        return ID;
    }

    public double getRedHigh() {
        return redHigh;
    }

    public double getYellowHigh() {
        return yellowHigh;
    }

    public double getYellowLow() {
        return yellowLow;
    }

    public double getRedLow() {
        return redLow;
    }

    public double getValue() {
        return value;
    }

    public Component getComponent() {
        return component;
    }
}
