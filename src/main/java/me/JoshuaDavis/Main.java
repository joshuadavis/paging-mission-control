package me.JoshuaDavis;

import me.JoshuaDavis.manager.SatelliteManager;

import java.io.File;

public class Main {

    private static SatelliteManager manager;

    public static void main(String[] args) {
        manager = new SatelliteManager();
        manager.ingest(new File("src/main/resources/Input.txt"));
        manager.pushAlerts();
    }

    public static SatelliteManager getSatelliteManager() {
        return manager;
    }
}
